Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

IZMJENJENO 
raw_input ---> input 
print 'File cannot be opened:', fname ----> print ('File cannot be opened:', fname)
print counts ----> print (counts)
counts[word] = 1 ----> counts[word] += 1
fnamex ---> fname 