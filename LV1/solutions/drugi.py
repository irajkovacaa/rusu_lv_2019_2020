
while True:
  try:
     userInput = float(input("Enter number: "))       
  except ValueError:
     print("Not number!")
     continue
  else:
     print("Yes number!")
     break 

if(userInput>=0.9):
    print("A")
elif(userInput>=0.8):
    print("B")
elif(userInput>=0.7):
    print("C")
elif(userInput>=0.6):
    print("D")
elif(userInput<0.6):
    print("F")