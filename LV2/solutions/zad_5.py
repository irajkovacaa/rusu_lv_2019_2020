import pandas as pd 
import matplotlib.pyplot as plt 


fig = plt.figure()  
ax = fig.add_subplot(1,1,1)     

mtcars = pd.read_csv('./rusu_lv_2019_2020/LV2/resources/mtcars.csv')  
#print mtcars
plt.scatter(mtcars.mpg, mtcars.hp)  
plt.xlabel("mpg")                   
plt.ylabel("hp")                    
plt.grid(linestyle='--')            

"""Dodavanje informacije o težini za svako vozilo"""
for i in range (0, len(mtcars.mpg)):        
    ax.annotate('%.2f' % mtcars.wt[i],      
                xy=(mtcars.mpg[i], mtcars.hp[i]), 
                xytext=(mtcars.mpg[i]+2, mtcars.hp[i]+20), 
                arrowprops=dict(arrowstyle="-", facecolor='black'), 
                )